package be.kdg.java2.sms.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.util.logging.Logger;

public class LoginView extends BorderPane {
    private static final Logger L = Logger.getLogger(LoginView.class.getName());
    private TextField tfUser;
    private PasswordField pfPassword;
    private Button btnLogin;
    private Button btnAdd;

    public LoginView() {
        L.info("Constructing LoginView");
        tfUser = new TextField();
        pfPassword = new PasswordField();
        btnAdd = new Button("Add");
        btnLogin = new Button("Login");
        GridPane gpCenter = new GridPane();
        gpCenter.add(new Label("User:"), 0, 0);
        gpCenter.add(tfUser, 1, 0);
        gpCenter.add(new Label("Password:"), 0, 1);
        gpCenter.add(pfPassword, 1, 1);
        gpCenter.setHgap(5);
        gpCenter.setVgap(5);
        super.setCenter(gpCenter);
        HBox hbBottom = new HBox(btnAdd, btnLogin);
        hbBottom.setSpacing(5);
        super.setBottom(hbBottom);
        BorderPane.setMargin(gpCenter, new Insets(10));
        BorderPane.setMargin(hbBottom, new Insets(10));
    }

    TextField getTfUser() {
        return tfUser;
    }

    PasswordField getPfPassword() {
        return pfPassword;
    }

    Button getBtnLogin() {
        return btnLogin;
    }

    Button getBtnAdd() {
        return btnAdd;
    }
}
