package be.kdg.java2.sms.view;

import be.kdg.java2.sms.exceptions.StudentException;
import be.kdg.java2.sms.service.StudentsService;
import be.kdg.java2.sms.service.UserService;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;

import java.util.logging.Logger;

public class LoginPresenter {
    private static final Logger L = Logger.getLogger(LoginPresenter.class.getName());
    private LoginView loginView;
    private UserService userService;

    public LoginPresenter(LoginView loginView, UserService userService) {
        this.loginView = loginView;
        this.userService = userService;
        addEventHandlers();
    }

    private void addEventHandlers() {
        loginView.getBtnAdd().setOnAction(event -> {
            L.info("Button add clicked...");
            try {
                userService.addUser(loginView.getTfUser().getText(), loginView.getPfPassword().getText());
            } catch (StudentException studentException) {
                new Alert(Alert.AlertType.ERROR, "There was a problem while adding the user:\n" + studentException.getMessage()).showAndWait();
            }
        });
        loginView.getBtnLogin().setOnAction(event -> {
            L.info("Button login clicked...");
            try {
                boolean result = userService.login(loginView.getTfUser().getText(), loginView.getPfPassword().getText());
                L.info("Result of login:" + result);
                if (result) {
                    StudentsView studentsView = new 	StudentsView();
                    StudentsService studentsService = new StudentsService();
                    new StudentsPresenter(studentsView, studentsService);
                    Scene scene = loginView.getScene();
                    scene.setRoot(studentsView);
                    scene.getWindow().sizeToScene();
                }
            } catch (StudentException studentException) {
                new Alert(Alert.AlertType.ERROR, "There was a problem while logging in:\n" + studentException.getMessage()).showAndWait();
            }
        });
    }
}
