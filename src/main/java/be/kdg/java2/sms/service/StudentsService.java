package be.kdg.java2.sms.service;

import be.kdg.java2.sms.database.StudentDAO;

import java.util.List;

public class StudentsService {
    private StudentDAO studentDAO;

    public StudentsService() {
        studentDAO = new StudentDAO();
    }

    public List<Student> getAllStudents(){
        return studentDAO.retreiveAll();
    }

    public void addStudent(Student student) {
        studentDAO.add(student);
    }
}
