package be.kdg.java2.sms;

import be.kdg.java2.sms.service.StudentsService;
import be.kdg.java2.sms.service.UserService;
import be.kdg.java2.sms.view.LoginPresenter;
import be.kdg.java2.sms.view.LoginView;
import be.kdg.java2.sms.view.StudentsPresenter;
import be.kdg.java2.sms.view.StudentsView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.logging.Logger;

public class Main extends Application {
    private static final Logger L = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        L.info("Starting Student Management System on thread: " + Thread.currentThread().getName());
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        L.info("Running start methode on thread: " + Thread.currentThread().getName());
        LoginView loginView = new LoginView();
        UserService userService = new UserService();
        new LoginPresenter(loginView, userService);
        primaryStage.setScene(new Scene(loginView));
        primaryStage.show();
    }
}
